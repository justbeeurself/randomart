import each from "lodash/each";

let defaultSymbols = {
    '-2': 'E', // end
    '-1': 'S', // start
    '0': ' ',
    '1': '.',
    '2': 'o',
    '3': '+',
    '4': '=',
    '5': '*',
    '6': 'B',
    '7': 'O',
    '8': 'X',
    '9': '@',
    '10': '%',
    '11': '&',
    '12': '#',
    '13': '/',
    '14': '^'
};

let special = {
    end: -2,
    start: -1,
    empty: 0
};

let defaultBounds = {
    width: 17,
    height: 9
};

function createBoard(bounds, value) {
    let result = [];

    for (let i = 0; i < bounds.width; i++) {
        result[i] = [];
        for (let j = 0; j < bounds.height; j++) {
            result[i][j] = value;
        }
    }

    return result;
}

function generateBoard(data, options = {}) {
    let bounds = options.bounds || defaultBounds;
    let board = createBoard(bounds, special.empty);

    let x = Math.floor(bounds.width / 2);
    let y = Math.floor(bounds.height / 2);

    board[x][y] = special.start;

    each(data, b => {
        for (let s = 0; s < 8; s += 2) {
            let d = (b >> s) & 3;

            switch (d) {
                case 0: // up
                case 1:
                    if (y > 0) y--;
                    break;
                case 2: // down
                case 3:
                    if (y < (bounds.height - 1)) y++;
                    break;
            }
            switch (d) {
                case 0: // left
                case 2:
                    if (x > 0) x--;
                    break;
                case 1: // right
                case 3:
                    if (x < (bounds.width - 1)) x++;
                    break;
            }

            if (board[x][y] >= special.empty)
                board[x][y]++;
        }
    });

    board[x][y] = special.end;

    return {
        board: board,
        bounds: bounds
    };
}

function boardToString(board, options = {}) {
    let symbols = options.symbols || defaultSymbols;

    let width = board.bounds.width;
    let height = board.bounds.height;

    let result = [];

    for (var i = 0; i < height; i++) {
        result[i] = [];
        for (var j = 0; j < width; j++) {
            result[i][j] = symbols[board.board[j][i]] || symbols[special.empty];
        }
        result[i] = result[i].join('');
    }

    return result.join('\n');
}


export default (data, options = {}) =>
    boardToString(generateBoard(data, options), options);